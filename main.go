package main

import (
	"bytes"
	"database/sql"
	"io/ioutil"
	"reflect"

	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/buaazp/fasthttprouter"
	_ "github.com/lib/pq"
	"github.com/likexian/whois-go"
	"github.com/valyala/fasthttp"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"
)

var myClient = &http.Client{Timeout: 10 * time.Second}
var internDataJson DomainInfo
var historyInterface History

const SSLLABS_URL string = "https://api.ssllabs.com/api/v3/analyze?host="
const COUNTRY_VALUE_FIND = "Country"
const COUNTRY_VALUE_FIND_ALT ="country"
const ORGNAME_VALUE_FIND = "OrgName"
const ORGNAME_VALUE_FIND_ALT = "org-name"
const TIME_LAYOUT ="2006-01-02T15:04:05.000000Z"
const MIN_TIME_REF_HOURS = 1

func Index(r *fasthttp.RequestCtx) {
	r.Response.Header.Set("Access-Control-Allow-Origin", "*")

	ruta:=r.Request.URI().String()
	u,_  := url.Parse(ruta)
	paths := strings.Split(u.Path,"/")

	domain:=paths[len(paths)-1]

	createInternJson(domain)
	insertDataToDB(domain)
	createHistoryJson(domain)

	fmt.Fprint(r, internDataJson)
}

func insertDataToDB(domain string){
	var idDomain string

	jsonDataInBytes,err:=json.Marshal(internDataJson)
	jsonReader := bytes.NewReader(jsonDataInBytes)
	res,err:=ioutil.ReadAll(jsonReader)

	db, err := sql.Open("postgres", "user=maxroach dbname=test sslmode=disable port=26257")
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}

	row,err:=db.Query(`SELECT id_domain FROM domains WHERE id_domain IN ('`+domain+`');`)
	if err != nil {
		log.Fatal(err)
	}
	defer row.Close()

	for row.Next(){
		if err := row.Scan(&idDomain); err != nil {
			log.Fatal(err)
		}
	}
	if idDomain!=""{
		if _,err:=db.Exec(
			`INSERT INTO domains (profile_id,last_updated,id_domain,domain_data) VALUES (gen_random_uuid(),NOW(),'`+domain+`','`+string(res)+`');`); err != nil {
			log.Fatal(err)
		}
	}
}

func createInternJson(domain string){
	domainInterface := Domain{}
	getExternJson(SSLLABS_URL+domain, &domainInterface)

	servers := []ServersInfo{}

	for i:=0;i< len(domainInterface.Endpoints);i++{
		s := ServersInfo{
			Address: domainInterface.Endpoints[i].IpAddress,
			Ssl_grade: domainInterface.Endpoints[i].Grade,
			Country: findCountryFromWhoIs(domainInterface.Endpoints[i].IpAddress),
			Owner: findOrgNameFromWhoIs(domainInterface.Endpoints[i].IpAddress),
		}
		servers = append(servers,s)
	}

	dataChanged:=isDataChanged(domain)
	previousSslGrade:=getPreviousSslGrade(domain)
	sslGrade:= getLowerSslGrade(servers)
	titlePage:=getTitleOfPage(domain)
	logoPage:=getLogoOfPage(domain)

	internDataJson = DomainInfo{servers, dataChanged,sslGrade,previousSslGrade,logoPage,titlePage}
}

func createHistoryJson(domain string){
	if domain!=""{
		var data string
		var historyData History

		db, err := sql.Open("postgres", "user=maxroach dbname=test sslmode=disable port=26257")
		if err != nil {
			log.Fatal("error connecting to the database: ", err)
		}

		row,err:=db.Query(`SELECT history_info FROM history`)
		if err != nil {
			log.Fatal(err)
		}
		defer row.Close()

		for row.Next(){
			if err := row.Scan(&data); err != nil {
				log.Fatal(err)
			}
		}

		if data!=""{
			if err := json.Unmarshal([]byte(data), &historyData); err != nil {
				log.Fatal(err)
			}

			if historyData.Domains!=nil{
				dh:=DomainHistory{domain,time.Now()}
				historyData.Domains=append(historyData.Domains,dh)
				historyInterface=historyData

				fmt.Println(historyInterface)
				jsonDataInBytes,err:=json.Marshal(&historyInterface)
				if err != nil {
					log.Fatal( err)
				}
				jsonReader := bytes.NewReader(jsonDataInBytes)
				res,err:=ioutil.ReadAll(jsonReader)
				if err != nil {
					log.Fatal( err)
				}

				if _, err := db.Exec("UPDATE history SET history_info = $1",string(res)); err != nil {
					log.Fatal(err)
				}
			}else{
				fmt.Println("no data")
			}
		}else{
			set:=[]DomainHistory{}
			dh:=DomainHistory{domain,time.Now()}
			set=append(set,dh)
			historyInterface=History{set}

			jsonDataInBytes,err:=json.Marshal(&historyInterface)
			if err != nil {
				log.Fatal( err)
			}
			jsonReader := bytes.NewReader(jsonDataInBytes)
			res,err:=ioutil.ReadAll(jsonReader)
			if err != nil {
				log.Fatal( err)
			}

			fmt.Println(string(res))

			if _,err:=db.Exec(
				`INSERT INTO history (id,history_info) VALUES (gen_random_uuid(),$1);`, string(res)); err != nil {
				log.Fatal(err)
			}
		}
	}
}

func getPreviousSslGrade(domain string)string{
	var previousTime string
	var jsonDataText string
	var jsonData DomainInfo
	var previousGrade string

	db, err := sql.Open("postgres", "user=maxroach dbname=test sslmode=disable port=26257")
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}

	row,err:=db.Query(`SELECT last_updated FROM domains WHERE id_domain IN ('`+domain+`');`)
	if err != nil {
		log.Fatal(err)
	}
	defer row.Close()

	for row.Next(){
		if err := row.Scan(&previousTime); err != nil {
			log.Fatal(err)
		}
	}
	if previousTime !=""{
		startT,err:=time.Parse(TIME_LAYOUT,previousTime)
		if err != nil {
			fmt.Println(err)
		}

		timeNow:=time.Now().UTC()
		difference:=timeNow.Sub(startT)
		diffHours:=difference.Hours()

		if diffHours>=MIN_TIME_REF_HOURS{
			row,err:=db.Query(`SELECT domain_data FROM domains WHERE id_domain IN ('`+domain+`');`)
			if err != nil {
				log.Fatal(err)
			}
			defer row.Close()

			for row.Next(){
				if err := row.Scan(&jsonDataText); err != nil {
					log.Fatal(err)
				}
			}
			if err := json.Unmarshal([]byte(jsonDataText), &jsonData); err != nil {
				log.Fatal(err)
			}

			previousGrade=jsonData.Ssl_grade

			if _, err := db.Exec("UPDATE domains SET last_updated = NOW() WHERE id_domain = $1","www.truora.com"); err != nil {
				log.Fatal(err)
			}
		}
	}
	return previousGrade
}

func isDataChanged(domain string)bool{
	var data string
	var jsonData DomainInfo
	var previousTime string

	result:=false

	db, err := sql.Open("postgres", "user=maxroach dbname=test sslmode=disable port=26257")
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}

	row2,err:=db.Query(`SELECT last_updated FROM domains WHERE id_domain IN ('`+domain+`');`)
	if err != nil {
		log.Fatal(err)
	}
	defer row2.Close()

	for row2.Next(){
		if err := row2.Scan(&previousTime); err != nil {
			log.Fatal(err)
		}
	}

	if previousTime !=""{
		startT,err:=time.Parse(TIME_LAYOUT,previousTime)
		if err != nil {
			fmt.Println(err)
		}

		timeNow:=time.Now().UTC()
		difference:=timeNow.Sub(startT)
		diffHours:=difference.Hours()
		if diffHours>=MIN_TIME_REF_HOURS{
			row2,err:=db.Query(`SELECT domain_data FROM domains WHERE id_domain IN ('`+domain+`');`)
			if err != nil {
				log.Fatal(err)
			}

			for row2.Next(){
				if err := row2.Scan(&data); err != nil {
					log.Fatal(err)
				}
			}

			if data!=""{
				if err := json.Unmarshal([]byte(data), &jsonData); err != nil {
					log.Fatal(err)
				}
				isEqual:=reflect.DeepEqual(jsonData,internDataJson)

				if isEqual==false{
					jsonDataInBytes,err:=json.Marshal(internDataJson)
					if err != nil {
						log.Fatal(err)
					}
					jsonReader := bytes.NewReader(jsonDataInBytes)
					res,err:=ioutil.ReadAll(jsonReader)
					if err != nil {
						log.Fatal(err)
					}

					if _, err := db.Exec("UPDATE domains SET last_updated = NOW() WHERE id_domain = $1",domain); err != nil {
						log.Fatal(err)
					}
					if _, err := db.Exec("UPDATE domains SET domain_data = $1 WHERE id_domain = $2",string(res),domain); err != nil {
						log.Fatal(err)
					}
					result=true
				}
			}

		}
	}
	return result
}

func getLowerSslGrade(servers []ServersInfo)string{
	lowerGradeIndex:=0

	for i:=1;i<len(servers);i++{
		if strings.Compare(servers[lowerGradeIndex].Ssl_grade,servers[i].Ssl_grade)<0 {
			lowerGradeIndex=i
		}
	}

	return servers[lowerGradeIndex].Ssl_grade
}

func getInternJsonResponse(r *fasthttp.RequestCtx){
	r.Response.Header.Set("Content-Type", "application/json")
	json.NewEncoder(r.Response.BodyWriter()).Encode(internDataJson)

}

func getHistoryJson(r *fasthttp.RequestCtx){
	var data string
	var historyData History

	db, err := sql.Open("postgres", "user=maxroach dbname=test sslmode=disable port=26257")
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}

	row,err:=db.Query(`SELECT history_info FROM history`)
	if err != nil {
		log.Fatal(err)
	}
	defer row.Close()

	for row.Next(){
		if err := row.Scan(&data); err != nil {
			log.Fatal(err)
		}
	}

	if data!="" {
		if err := json.Unmarshal([]byte(data), &historyData); err != nil {
			log.Fatal(err)
		}

		if historyData.Domains != nil {
			historyInterface = historyData
		}
	}

	fmt.Println(historyInterface)

	r.Response.Header.Set("Content-Type","application/json")
	json.NewEncoder(r.Response.BodyWriter()).Encode(historyInterface)
}

func findCountryFromWhoIs(ipAddress string)string{
	var value string

	result, err := whois.Whois(ipAddress)
	finalResult := ""

	if strings.Contains(result,COUNTRY_VALUE_FIND_ALT){
		value=COUNTRY_VALUE_FIND_ALT
	}else if strings.Contains(result,COUNTRY_VALUE_FIND){
		value=COUNTRY_VALUE_FIND
	}
	if err==nil{
		res := strings.Split(result,value+":")
		textBlock := res[1]
		valueLine := strings.Index(res[1],"\n")
		//res1 := bytes.Index([]byte(res[1]), []byte("\n"))
		finalResult= textBlock[:valueLine]
	}
	return finalResult
}

func findOrgNameFromWhoIs(ipAddress string)string{
	var value string

	result, err := whois.Whois(ipAddress)
	finalResult := ""

	if strings.Contains(result,ORGNAME_VALUE_FIND){
		value=ORGNAME_VALUE_FIND
	}else if strings.Contains(result,ORGNAME_VALUE_FIND_ALT){
		value=ORGNAME_VALUE_FIND_ALT
	}
	if err==nil{
		res := strings.Split(result,value+":")
		textBlock := res[1]
		valueLine := strings.Index(res[1],"\n")
		//res1 := bytes.Index([]byte(res[1]), []byte("\n"))
		finalResult= textBlock[:valueLine]
	}
	return finalResult
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func getExternJson(url string, target interface{}) error {

	r, err := myClient.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

func getTitleOfPage(domain string)string{
	var finalResultTitle string
	response, err := http.Get("https://"+domain)

	if err != nil {
		finalResultTitle="Error getting HTTP response."
		log.Fatal(err)
	}else{
		defer response.Body.Close()

		doc, err := goquery.NewDocumentFromReader(response.Body)
		if err != nil {
			finalResultTitle="Error loading HTTP response body."
		}else{
			finalResultTitle=doc.Find("title").Text()
		}
	}
	return finalResultTitle

}

func getLogoOfPage(domain string)string{
	var finalResultLogo string
	response, err := http.Get("https://"+domain)

	if err != nil {
		finalResultLogo="Error getting HTTP response."
		log.Fatal(err)
	}else{
		defer response.Body.Close()

		doc, err := goquery.NewDocumentFromReader(response.Body)
		if err != nil {
			finalResultLogo="Error loading HTTP response body."
		}else{
			doc.Find("meta").Each(func(i int, s *goquery.Selection) {
				if property, _ := s.Attr("property"); property == "og:image" {
					ogImage, _ := s.Attr("content")
					finalResultLogo = ogImage
				}
			})
		}
	}
	return finalResultLogo
}



func main() {
	router := fasthttprouter.New()

	router.GET("/search/:domain", Index)
	router.GET("/json",getInternJsonResponse)
	router.GET("/json/history",getHistoryJson)

	//handler := fasthttp.FSHandler("./public", 0)
	//router.Handle("GET", "/", handler)

	//createInternJson("www.truora.com")

	//jsonDataInBytes,err:=json.Marshal(internDataJson)
	//jsonReader := bytes.NewReader(jsonDataInBytes)
	//res,err:=ioutil.ReadAll(jsonReader)

	/*
	db, err := sql.Open("postgres", "user=maxroach dbname=test sslmode=disable port=26257")
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}*/
/*
	if _,err:=db.Exec(
		`INSERT INTO domains (profile_id,last_updated,id_domain,domain_data) VALUES (gen_random_uuid(),NOW(),'www.truora.com','`+string(res)+`');`); err != nil {
		log.Fatal(err)
	}*/


	log.Fatal(fasthttp.ListenAndServe(":3000", router.Handler))
}

type History struct{
	Domains []DomainHistory
}

type DomainHistory struct{
	Domain string
	DateTime time.Time
}

type DomainInfo struct{
	Servers []ServersInfo
	Servers_changed bool
	//Is_down bool
	Ssl_grade string
	Previous_ssl_grade string
	Logo string
	Tittle string
}

type ServersInfo struct{
	Address string
	Ssl_grade string
	Country string
	Owner string
}

type Domain struct {
	Host, Protocol, Status, EngineVersion, CriteriaVersion string
	Port                                                   int
	IsPublic                                               bool
	StartTime                                              int
	TestTime                                               int
	Endpoints                                              []EndpointsInfo
}

type EndpointsInfo struct {
	IpAddress, StatusMessage, Grade string
}
