El servidor del fronted debe ser iniciado desde la carpeta frontend del proyecto: npm run serve.
Es preferible que se abra en el navegador por red, pues de forma local en ese respectivo puerto (8080) se abrirá un entorno visual de cockroach DB

El servidor del backend estará escuchando desde el puero 3000.

Es necesario iniciar el cluster de cockroach DB dentro de la carpeta raiz del proyecto: cockroach start --insecure
 